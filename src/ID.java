public enum ID {
    PlayerCharacter(),
    Block(),
    Enemy(),
    Bullet(),
    BulletDiamond(),
    LifePotion()
}
