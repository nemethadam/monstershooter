import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class Game extends Canvas implements Runnable{

    private static final long serialVersionUID = 1L;

    private boolean isRunning = false;
    private Thread thread;
    private Handler handler;
    private Camera camera;
    private SpriteSheet ss;

    private Window window;
    private BufferedImage level = null;
    private BufferedImage sprite_sheet = null;
    private BufferedImage floor = null;
    private KeyInput keyInput;
    private MouseInput mouseInput;


    public int ammo = 100;
    public static int hp = 100;
    public static int numberOfEnemies;
    public String status = "";


    public Game(){
        window = new Window(1000, 563,"Monster shooter", this);
        start();

        handler = new Handler();
        camera = new Camera(0, 0);
        keyInput = new KeyInput(handler, this);
        mouseInput = new MouseInput(handler, camera, this, ss);
        this.addKeyListener(keyInput);

        BufferedImageLoader loader = new BufferedImageLoader();
        level = loader.loadImage("/level.png");
        sprite_sheet = loader.loadImage("/sprite_sheet2.png");
        ss = new SpriteSheet(sprite_sheet);
        floor = ss.grabImage(4, 2, 32, 32);
        this.addMouseListener(new MouseInput(handler, camera, this, ss));
        loadLevel(level);
    }

    public static int getHp() {
        return hp;
    }

    public static int getNumberOfEnemies() {
        return numberOfEnemies;
    }

    private void start(){
        isRunning = true;
        thread = new Thread(this);
        thread.start();
    }

    private void stop(){
        isRunning = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void restart(){
        //TODO: implement
    }

    public void run() {
        this.requestFocus();
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int frames = 0;
        while (isRunning){
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while (delta >=1){
                tick();
                delta--;
            }
            render();
            frames++;

            if(System.currentTimeMillis() - timer > 1000){
                timer += 1000;
                frames = 0;
            }
        }
        stop();
    }

    public void tick(){
        for (int i = 0; i < handler.object.size(); i++) {
            if (handler.object.get(i).getId() == ID.PlayerCharacter){
                camera.tick(handler.object.get(i));
            }
        }
        handler.tick();
    }

    public void render(){
        BufferStrategy bs = this.getBufferStrategy();
        if(bs == null){
            this.createBufferStrategy(3);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(-camera.getX(), -camera.getY());
        for (int xx = 0; xx < 30*72; xx+=32) {
            for (int yy = 0; yy < 30*72; yy+=32) {
                g.drawImage(floor, xx, yy, null);
            }
        }
        handler.render(g);
        g2d.translate(camera.getX(), camera.getY());

        g.setColor(Color.gray);
        g.fillRect(5, 5, 200, 32);
        g.setColor(Color.green);
        g.fillRect(5, 5, hp*2, 32);
        g.setColor(Color.black);
        g.drawRect(5, 5, 200, 32);

        g.setColor(Color.white);
        g.drawString("Ammo: " + ammo, 5, 50);
        g.drawString("Number of enemies: " + numberOfEnemies, 5, 65);
        g.drawString("Move: AWSD or arrow keys. Shoot: mouse left button", 5, 530);

        if(status.equals("Died")){
            g.drawString("Press space to exit", 5, 150);
            g.setFont(new Font("Times New Roman", Font.BOLD, 50));
            g.drawString("You loose", 5, 120);
            stopWizardMove();
        }

        if(status.equals("Win")){
            g.drawString("Press space to exit", 5, 150);
            g.setFont(new Font("Times New Roman", Font.BOLD, 50));
            g.drawString("You WIN", 5, 120);
            stopWizardMove();
        }

        g.dispose();
        bs.show();
    }

    public void checkStatus(){
        if (numberOfEnemies <= 0){
            status = "Win";
        }
        if (hp <= 0){
            status = "Died";
        }
    }

    public void stopWizardMove(){
        handler.setUp(false);
        handler.setDown(false);
        handler.setLeft(false);
        handler.setRight(false);
    }

    //loading level
    private void loadLevel(BufferedImage image){
        int w = image.getWidth();
        int h = image.getHeight();

        for (int xx = 0; xx < w; xx++) {
            for (int yy = 0; yy < h; yy++) {
                int pixel = image.getRGB(xx, yy);
                int red = (pixel >> 16) & 0xff;
                int green = (pixel >> 8) & 0xff;
                int blue = (pixel) & 0xff;

                if (red == 255 && green == 0 && blue == 0)
                    handler.addObject(new Block(xx*32, yy*32, ID.Block, ss));

                if (blue == 255 && green == 0 && red == 0)
                    handler.addObject(new PlayerCharacter(xx*32, yy*32, ID.PlayerCharacter, handler, this, ss));

                if (green == 255 && blue == 0 && red == 0){
                    handler.addObject(new Enemy(xx*32, yy*32, ID.Enemy, handler, this, ss));
                    ++numberOfEnemies;
                }

                if (green == 255 && blue == 255 & red == 0)
                    handler.addObject(new BulletDiamond(xx*32, yy*32, ID.BulletDiamond, ss));

                if (red == 255 && blue == 255 && green==0){
                    handler.addObject(new LifePotion(xx*32, yy*32, ID.LifePotion, ss));
                }
            }
        }
    }

    public static void main(String[] args) {
        new Game();
    }
}
